## Парадигма

- Именование классов по БЭМ, разметка в [pug](https://pugjs.org/) и стилизация [SCSS](http://sass-lang.com/). См. [Как работать с CSS-препроцессорами и БЭМ](http://nicothin.github.io/idiomatic-pre-CSS/)
- Каждый БЭМ-компонент в своём компоненте внутри `./src/components/`

## Разметка

Используется [pug](https://pugjs.org/api/getting-started.html).

## Стили

Используется [SCSS](http://sass-lang.com/).

## Компоненты

Каждый компонент лежит в `./src/components/` в своем файле.

Для именований компонентов используем документацию [Vue](https://ru.vuejs.org/v2/style-guide/index.html#%D0%A4%D0%B0%D0%B9%D0%BB%D1%8B-%D0%BA%D0%BE%D0%BC%D0%BF%D0%BE%D0%BD%D0%B5%D0%BD%D1%82%D0%BE%D0%B2-%D0%BD%D0%B0%D1%81%D1%82%D0%BE%D1%8F%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE-%D1%80%D0%B5%D0%BA%D0%BE%D0%BC%D0%B5%D0%BD%D0%B4%D1%83%D0%B5%D1%82%D1%81%D1%8F)

## Страницы

Каждая страница находитися в `src/views/`
