import Vue from "vue";
import Router from "vue-router";
import TavernaIndex from "./views/Taverna/Index.vue";
import TavernaPurchase from "./views/Taverna/Purchase.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/taverna",
      name: "TavernaIndex",
      component: TavernaIndex
    },
    {
      path: "/taverna/purchase",
      name: "TavernaPurchase",
      component: TavernaPurchase
    }
  ]
});
