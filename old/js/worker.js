var dataPrice = [
    ['Орк', 2, 35],
    ['Норд', 3, 10],
    ['Одинокий редгард', 20, 14],
    ['Тёмный эльф', 10, 99],
    ['Аргонианен', 7, 149],
    
];

var newTable = document.createElement("table");
newTable.id = "tab";
for (var rows = 0; rows < dataPrice.length; rows++) {
    var newRow = newTable.insertRow(rows);
    newRow.classList.add("row" + rows % 2);
    for (var cell = 0; cell < 3; cell++) {
        var newCell = newRow.insertCell(cell);
        switch (cell) {
            case 0:
                newCell.classList.add("prod_name");
                newCell.innerHTML = '<div>•</div>' +  dataPrice[rows][cell];
                break;
            case 1:
                newCell.classList.add("input_val");
                var input = document.createElement("input");
                input.type = "text";
                input.maxLength = 6;
                input.className = "input_form";
                input.value = dataPrice[rows][cell];
                newCell.appendChild(input);

                var text = document.createElement('div');
                var img = document.createElement('div');
                img.classList.add('septim_img');
                text.classList.add('septim_text');
                text.textContent = 'в час';
                newCell.appendChild(img);
                newCell.appendChild(text);
                break;
            case 2:
                newCell.classList.add("btn");
                var btn = document.createElement("button");
                btn.textContent = 'Уволить';
                newCell.appendChild(btn);
                
                break;
        }
    }
}

wrapper.appendChild(newTable);