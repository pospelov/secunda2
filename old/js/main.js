var dataPrice = [
    [35, 0],
    [10, 0],
    [20, 1],
    [10, 1],
    [7, 0],
    [5, 1],
    [12, 0],
    [14, 0],
    [20, 1],
];

var newTable = document.createElement("table");
newTable.id = "tab";
for (var rows = 0; rows < dataPrice.length; rows++) {
    var newRow = newTable.insertRow(rows);
    newRow.classList.add("row" + rows % 2);
    for (var cell = 0; cell < 3; cell++) {
        var newCell = newRow.insertCell(cell);
        switch (cell) {
            case 0:
                newCell.classList.add("prod_name");
                var status, statusClass;
                if (dataPrice[rows][cell + 1] == 0) {
                    status = 'Свободно';
                    statusClass = 'online';
                }
                else {
                    status = 'Занято';
                    statusClass = 'offline';
                }
                newCell.innerHTML = '<div class="' + statusClass + '">•</div>' + 'Комната №' + (rows + 1) + '<span> - ' + status + '</span>';
                break;
            case 1:
                newCell.classList.add("input_val");
                var input = document.createElement("input");
                input.type = "text";
                input.maxLength = 6;
                input.className = "input_form";
                // if (getComputedStyle(document.getElementById("warning_status")).display != 'none')
                    input.value = dataPrice[rows][cell-1];
                // else input.value = dataPrice[rows][cell];
                newCell.appendChild(input);
                break;
            case 2:
                newCell.classList.add("septim_img");
                break;
        }
    }
}

wrapper.appendChild(newTable);