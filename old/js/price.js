var dataPrice = [
    ['Помидор', 20, 14],
    ['Вино', 10, 99],
    ['Зелье бодрости', 7, 149],
    ['Микстура здоровья', 5, 229],
    ['Черновересковый мёд', 12, 149],
    ['Эль', 14, 139],
    ['Капуста', 20, 14],
    ['Морковь', 24, 99],
    ['Тыква', 7, 149],
    ['Зелёное яблоко', 31, 9],
    ['Красное яблоко', 12, 11],
    ['Картофель', 14, 5],
    ['Куриная грудка', 5, 49],
    ['Вино «Альто»', 10, 99],
    ['Зелье бодрости', 7, 149],
    ['Микстура здоровья', 2, 224],
    ['Черновересковый мёд', 12, 139],
    ['Эль', 14, 129],
    ['Помидор', 20, 14],
    ['Вино', 10, 99],
    ['Зелье бодрости', 7, 149],
    ['Микстура здоровья', 2, 224],
    ['Черновересковый мёд', 12, 139],
    ['Эль', 14, 129],
];

var newTable = document.createElement("table");
newTable.id = "tab";
for (var rows = 0; rows < dataPrice.length; rows++) {
    var newRow = newTable.insertRow(rows);
    newRow.classList.add("row" + rows % 2);
    for (var cell = 0; cell < 3; cell++) {
        var newCell = newRow.insertCell(cell);
        switch (cell) {
            case 0:
                newCell.classList.add("prod_name");
                newCell.innerHTML =  dataPrice[rows][cell] + '<span> - ' + dataPrice[rows][cell+1] + ' шт. в наличии</span>';
                break;
            // case 1:
            //     newCell.classList.add('presence');
            //     newCell.innerHTML = dataPrice[rows][cell] + ' шт.' +  '<span> в наличии</span>';
            //     break;
            case 1:
                newCell.classList.add("input_val");
                var input = document.createElement("input");
                input.type = "text";
                input.maxLength = 6;
                input.className = "input_form";
                if (getComputedStyle(document.getElementById("warning_status")).display != 'none')
                    input.value = Math.ceil(dataPrice[rows][cell + 1] / 2);
                else input.value = dataPrice[rows][cell + 1];
                newCell.appendChild(input);
                break;
            case 2:
                newCell.classList.add("septim_img");
                break;
        }
    }
}

wrapper.appendChild(newTable);

var warning_visible = getComputedStyle(document.getElementById("warning_status"));
if (warning_visible.display == "none") document.getElementById("tab").style.height = "425px";
else document.getElementById("tab").style.height = "355px";